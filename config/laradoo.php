<?php

return [
    'api-suffix' => 'xmlrpc',     // 'xmlrpc' from version 7.0 and earlier, 'xmlrpc/2' from version 8.0 and above.

    //Credentials
    'host'       => 'http://impetus.mahavirconsultants.com',
    'db'         => 'impetus',
    'username'   => 'admin',
    'password'   => 'admin',
];