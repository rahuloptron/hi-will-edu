<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});


Route::get('/index.html', function () {
    return view('index');
});

Route::get('/about.html', function () {
    return view('about');
});

Route::get('/gallery.html', function () {
    return view('gallery');
});

Route::get('/contact.html', function () {
    return view('contact');
});

Route::get('/enroll-now.html', function () {
    return view('enroll-now');
});

Route::get('/courses/certificate-course-in-cad-cam-unigraphics-nx.html', function () {
    return view('/courses/certificate-course-in-cad-cam-unigraphics-nx');
});

Route::get('/courses/certificate-course-cad-unigraphics-nx-plastic-mold-design.html', function () {
    return view('/courses/certificate-course-cad-unigraphics-nx-plastic-mold-design');
});

Route::get('/courses/certificate-course-cam-unigraphics-nx.html', function () {
    return view('/courses/certificate-course-cam-unigraphics-nx');
});

Route::get('/courses/certificate-course-cam-delcam-powermill.html', function () {
    return view('/courses/certificate-course-cam-delcam-powermill');
});

Route::get('/courses/autocad-lt-2017.html', function () {
    return view('/courses/autocad-lt-2017');
});

Route::get('/courses/cnc-operating-and-programming.html', function () {
    return view('/courses/cnc-operating-and-programming');
});

Route::get('/thank-you.html', function () {
    return view('/thank-you');
});


Route::post('/send','FormController@send');

//Route::resource('/index2/{id}','indexController');

Route::resource('/create','CreateBlogController');
Route::get('/blog/{id}','CreateBlogController@show');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('admin', 'Admin\AdminController@index');
Route::resource('admin/roles', 'Admin\RolesController');
Route::resource('admin/permissions', 'Admin\PermissionsController');
Route::resource('admin/users', 'Admin\UsersController');
Route::get('admin/generator', ['uses' => '\Appzcoder\LaravelAdmin\Controllers\ProcessController@getGenerator']);
Route::post('admin/generator', ['uses' => '\Appzcoder\LaravelAdmin\Controllers\ProcessController@postGenerator']);


/**
 * Catchall route
 */
/*

  Route::get('{view}', function ($view) {
    try {
      return view($view);
    } catch (\Exception $e) {
      abort(404);
    }
  })->where('view', '.*');*/

Route::resource('products', 'productsController');
Route::resource('categories', 'categoriesController');

Route::get('/listing', 'productsController@productShowing');
Route::get('/final/{id}', 'productsController@productFinal');

Route::resource('inquiry', 'inquiryController');
