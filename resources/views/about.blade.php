@extends('layouts.app')
@section('content')

<div class="page_title1">
  <div class="container">
    <div class="title"><h1>About</h1></div>
       
  </div>
</div>

<div class="container">

  <div class="content_fullwidth">

  <div class="two_third">
    
       
        <h2>Who <strong>We Are</strong></h2>
        
        <p><strong>In today’s world of highly specialized products, general software solutions fail to meet the needs of fast-moving companies in competitive markets. Extensive software customization efforts along with complex, time-consuming implementations make it difficult for companies to quickly capitalize on the potential benefits these solutions can offer. Only by working with solutions built for the particular processes and in the specific language of an individual industry can companies leverage these technologies at the speed that today’s products demand.</strong></p>
    <br>
        <p>NX formerly known as Unigraphics is powerful modeling software having integrated capabilitiesof product design, tool design, engineering and manufacturing solutions which helps engineers todeliver excellent products faster and efficiently. NX CAD is basically parametric modeling software,with unique and technologically advanced features like synchronous modeling. For more about NX you can visit Siemens PLM Software web-site.</p>
    
    </div><!-- end section -->
    
    
    <div class="one_third last">
      
        <h2>Our <strong>Team</strong></h2>
 

    <img src="images/gallery/26.jpg" alt="">

</div><!-- progress bars section -->
    
  <div class="clearfix"></div>
    <div class="divider_line2"></div>

      
  <div class="two_third">
      <h2>What we do?</h2> 
      <div class="fullimage_box">
            <ul class="list5">
        <li>Doing need assessment for manpower in various industries.</li>
        <li>Performing skill gap analysis for various sectors.</li>
        <li>Preparing training program based on these analysis.</li>
        <li>Training to students, engineers and employees of company, industrialists, etc.</li>
        <li>All courses are regularly updates and changes are made as per industrial scenario.</li>
        <li>Currently we are doing skill base training on various subjects like CAD, CAM, Die Designing and CNC Programming.</li>
        <li>The courses are designed as per the industrial requirement.</li>
        <li>Placement of the trainees and after training support.</li>
   
            </ul>      
      </div>

        </div><!-- end about site area -->
         
     <div class="one_third last"> 
      <h2>What do we offer?</h2> 
      <div class="fullimage_box">
            <ul class="list5">
        <li>Unigraphics NX CAD</li> 
        <li>1Unigraphics NX CAM</li>    
        <li>Delcam Powermill</li>    
        <li>Solid Edge CAD</li>     
        <li>AutoCAD for Mechanical and Civil</li> 
        <li>CNC Programming</li> 
        <li>Die Designing</li>
        <li>Consultancy</li>
        <li>Industrial training on sales, quality and productivity.</li>
            </ul>      
      </div>      
    </div>  
             

</div>

</div>


</div>

@stop