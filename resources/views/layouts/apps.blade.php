<!doctype html>
<html lang="en-gb" class="no-js"> 
<head>
     @include('includes.head2')
</head>

<body>
<div class="site_wrapper">
<div class="container_full">    
 @include('includes.header')
</div>

@yield('content')


 @include('includes.sidebar')
</div>

@include('includes.footer')

</div>

@include('includes.js2')

</body>
</html>