<!doctype html>
<html lang="en-gb" class="no-js"> 
<head>
     @include('includes.head')
</head>

<body>
<div class="site_wrapper">
<div class="container_full">    
 @include('includes.header')
</div>

@yield('content')

@include('includes.footer')

</div>

@include('includes.js')

</body>
</html>