<!DOCTYPE html>
<html>
<head>
<title></title>
<link href="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.css" rel="stylesheet">
<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script> 
<script src="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.js"></script> 

<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet">
<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script>

</head>
<body>

<div class="container">

{{Form::open(['action' => 'CreateBlogController@store'])}}

 <div class="box-body">
 <div class="form-group">
 {{Form::label('title', 'Title')}}
 {{Form::text('title',null,array('class' => 'form-control', 'placeholder'=>'Title'))}}
 </div>
 <div class="form-group">
 {{Form::label('body', 'Content')}}
 {{Form::textarea('body',null,array('class' => 'form-control', 'placeholder'=>'Content', 'id' => 'technig'))}}
 </div>
 <div class="form-group">
     {{Form::submit('Publish Post',array('class' => 'btn btn-primary btn-sm'))}} </div>
{{Form::close()}} 

</div>


<script>
$(document).ready(function() {
            $('#technig').summernote({
              height:300,
            });
        });
</script>

</body>
</html> 



