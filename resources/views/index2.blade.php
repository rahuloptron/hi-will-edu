<!DOCTYPE html>
<html>
<head>
	<title>Fetch record from odoo with laravel xml rpc</title>
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
</head>
<body>
 
 <div class="container">

	<div class="row">

		
		
  		<div class="col-sm-3">
  			
  			<h1>Categories</h1>

  			<ul class="list">
  				
				@foreach ($product_category as $productsCat)

  				
				       <li> <a href="/index/{{ $productsCat['name'] }}">{{ $productsCat['name'] }}</a>	</li>
				  

				@endforeach

  			</ul>

  		</div>

  		<div class="col-sm-9">

			<h1>Fetch record from oddoo crm with laradoo plugin</h1>

			<hr>

			@foreach ($product_list as $products)

			<p>Product Name : {{ $products['display_name'] }}</p>
			<p>Product Desc : {{ $products['description'] }}</p>
			<p>Product Price : {{ $products['list_price'] }}</p>
			<p>Product Type : {{ $products['type'] }}</p>
			<p>Image : 

			<img src="data:image/png;base64,{{$products['image'] }}">

			</p>

			
		<hr>

		@endforeach
			

		</div>

		
	</div>

	</div>

</body>
</html>
