@extends('layouts.app')
@section('content')

<div class="page_title1">

	<div class="container">
		<div class="title"><h1>Gallery</h1></div>
      
	</div>
</div>

<div class="clearfix"></div>   
 


<div class="container">

<div class="content_fullwidth">
    
    <h2>Images of <strong>Team</strong></h2>
        
    <div class="one_third">
          <div class="portfolio_image">
            <i class="fa fa-search fa-4x"></i>
            <a class="fancybox" href="images/gallery/26.jpg" data-fancybox-group="gallery" title="Publishing has packages"><img src="images/gallery/26.jpg" alt=""></a>
            
            </div>
        </div><!-- end section -->
        
        <div class="one_third">
          <div class="portfolio_image">
            <i class="fa fa-search fa-4x"></i>
            <a class="fancybox" href="images/gallery/24.jpg" data-fancybox-group="gallery" title="Desktop publish packages"><img src="images/gallery/24.jpg" alt=""></a>
            
            </div>
        </div><!-- end section -->
        
        <div class="one_third last">
          <div class="portfolio_image">
            <i class="fa fa-search fa-4x"></i>
            <a class="fancybox" href="images/gallery/22.jpg" data-fancybox-group="gallery" title="Prity have suffered alteration form"><img src="images/gallery/22.jpg" alt=""></a>
            
            </div> 
        </div><!-- end section -->
        
        <div class="clearfix mar_top5"></div>

        <h2>Images of <strong>Training</strong></h2>
        
        <div class="one_third">
          <div class="portfolio_image">
            <a class="fancybox-media" href="images/gallery/14.jpg" title="Majority have alteration"><img src="images/gallery/14.jpg" alt=""></a>
            
            </div>
        </div><!-- end section -->
        
        <div class="one_third">
          <div class="portfolio_image">
            <a class="fancybox-media" href="images/gallery/13.jpg" title="Available but the majority"><img src="images/gallery/13.jpg" alt=""></a>
            
            </div>
        </div><!-- end section -->
        
        <div class="one_third last">
          <div class="portfolio_image">
            <i class="fa fa-search fa-4x"></i>
            <a class="fancybox" href="images/gallery/15.jpg" data-fancybox-group="gallery" title="Slteration suffer ation"><img src="images/gallery/15.jpg" alt=""></a>
            
            </div> 
        </div><!-- end section -->
    
         <div class="clearfix mar_top5"></div>
        
        <div class="one_third">
          <div class="portfolio_image">
            <a class="fancybox-media" href="images/gallery/17.jpg" title="Majority have alteration"><img src="images/gallery/17.jpg" alt=""></a>
            
            </div>
        </div><!-- end section -->
        
        <div class="one_third">
          <div class="portfolio_image">
            <a class="fancybox-media" href="images/gallery/18.jpg" title="Available but the majority"><img src="images/gallery/18.jpg" alt=""></a>
            
            </div>
        </div><!-- end section -->
        
        <div class="one_third last">
          <div class="portfolio_image">
            <i class="fa fa-search fa-4x"></i>
            <a class="fancybox" href="images/gallery/16.jpg" data-fancybox-group="gallery" title="Slteration suffer ation"><img src="images/gallery/16.jpg" alt=""></a>
            
            </div> 
        </div><!-- end section -->

         <div class="clearfix mar_top5"></div>
  </div>

</div>

@stop