<div class="form-group {{ $errors->has('productName') ? 'has-error' : ''}}">
    {!! Form::label('productName', 'Productname', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('productName', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
        {!! $errors->first('productName', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('productDesc') ? 'has-error' : ''}}">
    {!! Form::label('productDesc', 'Productdesc', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::textarea('productDesc', null, ('' == 'required') ? ['class' => 'form-control editor', 'required' => 'required'] : ['class' => 'form-control editor']) !!}
        {!! $errors->first('productDesc', '<p class="help-block">:message</p>') !!}
    </div>
</div>


<div class="form-group {{ $errors->has('productPrice') ? 'has-error' : ''}}">
    {!! Form::label('productPrice', 'Productprice', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('productPrice', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
        {!! $errors->first('productPrice', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('productModel') ? 'has-error' : ''}}">
    {!! Form::label('productModel', 'Productmodel', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('productModel', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
        {!! $errors->first('productModel', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('productSKU') ? 'has-error' : ''}}">
    {!! Form::label('productSKU', 'productSKU', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('productSKU', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
        {!! $errors->first('productSKU', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('productBarcode') ? 'has-error' : ''}}">
    {!! Form::label('productBarcode', 'productBarcode', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('productBarcode', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
        {!! $errors->first('productModel', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('productModel') ? 'has-error' : ''}}">
    {!! Form::label('productImage', 'productImage', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
         {!! Form::file('productImage', ['id'=>'image', 'class' => 'form-control']) !!}
      
    </div>
</div>

<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-primary']) !!}
    </div>
</div>
