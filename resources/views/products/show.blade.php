@extends('layouts.backend')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">product {{ $product->id }}</div>
                    <div class="card-body">

                        <a href="{{ url('/products') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <a href="{{ url('/products/' . $product->id . '/edit') }}" title="Edit product"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['products', $product->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger btn-sm',
                                    'title' => 'Delete product',
                                    'onclick'=>'return confirm("Confirm delete?")'
                            ))!!}
                        {!! Form::close() !!}
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $product->id }}</td>
                                    </tr>
                                    <tr><th> ProductName </th><td> {{ $product->productName }} </td></tr><tr><th> ProductDesc </th><td><?php echo $product->productDesc ;?> </td></tr>
                                    <tr>
                                        <th>ProductPrice</th><td>{{ $product->productPrice }}</td>
                                    </tr>
                                    <tr>
                                        <th>ProductModel</th><td>{{ $product->productModel }}</td>
                                    </tr>
                                    <tr>
                                        <th>ProductSKU</th><td>{{ $product->productSKU }}</td>
                                    </tr>
                                    <tr>
                                        <th>ProductBarcode</th><td>{{ $product->productBarcode }}</td>
                                    </tr>
                                    <tr>
                                        <th>ProductImage</th><td><img src="/public/images/productImage/{{ $product->productImage }}"></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
