
<div class="form-group {{ $errors->has('contactName') ? 'has-error' : ''}}">
    {!! Form::label('contactName', 'Contactname', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('contactName', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
        {!! $errors->first('contactName', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('contactEmail') ? 'has-error' : ''}}">
    {!! Form::label('contactEmail', 'Contactemail', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('contactEmail', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
        {!! $errors->first('contactEmail', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('contactMobile') ? 'has-error' : ''}}">
    {!! Form::label('contactMobile', 'Contactmobile', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('contactMobile', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
        {!! $errors->first('contactMobile', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('productName') ? 'has-error' : ''}}">
    {!! Form::label('productName', 'Productname', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
            <select class="form-control" name="productName">
                @foreach ($product_list as $products)
                <option value="{{ $products['id'] }}">{{ $products['name'] }}</option>
                @endforeach

            </select>
    </div>
</div>

<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-primary']) !!}
    </div>
</div>



                          

                      


