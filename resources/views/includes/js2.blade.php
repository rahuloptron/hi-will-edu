<!-- style switcher -->
<script type="text/javascript" src="../js/universal/jquery.js"></script>

<!-- main menu -->
<script type="text/javascript" src="../js/mainmenu/ddsmoothmenu.js"></script>
<script type="text/javascript" src="../js/mainmenu/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="../js/mainmenu/selectnav.js"></script>

<!-- progress bar -->
<script src="../js/progressbar/progress.js" type="text/javascript" charset="utf-8"></script>
  
<!-- jquery jcarousel -->
<script type="text/javascript" src="../js/jcarousel/jquery.jcarousel.min.js"></script>

<script type="text/javascript" src="../js/mainmenu/scripts.js"></script>

<!-- top show hide plugin script-->
<script src="../js/show-hide-plugin/showHide.js" type="text/javascript"></script>

<!-- scroll up -->
<script type="text/javascript">
    $(document).ready(function(){
 
        $(window).scroll(function(){
            if ($(this).scrollTop() > 100) {
                $('.scrollup').fadeIn();
            } else {
                $('.scrollup').fadeOut();
            }
        });
 
        $('.scrollup').click(function(){
            $("html, body").animate({ scrollTop: 0 }, 500);
            return false;
        });
 
    });
</script>

<!-- jquery jcarousel -->
<script type="text/javascript">

    jQuery(document).ready(function() {
            jQuery('#mycarousel').jcarousel();
    });
    
    jQuery(document).ready(function() {
            jQuery('#mycarouseltwo').jcarousel();
    });
    
    jQuery(document).ready(function() {
            jQuery('#mycarouselthree').jcarousel();
    });
    
    jQuery(document).ready(function() {
            jQuery('#mycarouselfour').jcarousel();
    });
    
</script>

<!-- accordion -->
<script type="text/javascript" src="../js/accordion/custom.js"></script>